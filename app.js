const express = require('express')
// 导入 cors 中间件
const cors = require('cors')
const userRouter = require('./router/user')
const userInfoRouter = require('./router/userInfo')
const artCateRouter = require('./router/artcate')
const articleRouter = require('./router/article')
const expressJWT = require('express-jwt')
// 导入配置文件
const config = require('./config/config')

const joi = require('joi')


const app = express()

// 托管静态资源文件
app.use('/uploads', express.static('./uploads'))

// 将 cors 注册为全局中间件
app.use(cors())
// 解析表单数据的中间件
app.use(express.urlencoded({ extended: false }))

// 身份认证  .unless({ path: [/^\/api\//] }) 指定哪些接口不需要进行 Token 的身份认证
app.use(expressJWT({ secret: config.jwtSecretKey }).unless({ path: [/^\/api\//] }))

// 响应数据的中间件
app.use(function (req, res, next) {
    // status = 0 为成功； status = 1 为失败； 默认将 status 的值设置为 1，方便处理失败的情况
    res.cc = function (err, status = 1) {
        res.send({
            // 状态
            status,
            // 状态描述，判断 err 是 错误对象 还是 字符串
            message: err instanceof Error ? err.message : err,
        })
    }
    next()
})

// 错误响应中间件
app.use(function (err, req, res, next) {
    next()
    // 捕获身份认证失败的错误
    if (err.name === 'UnauthorizedError') return res.cc('身份认证失败！')
    // 数据验证失败
    if (err instanceof joi.ValidationError) return res.cc(err)
    // 未知错误
    res.cc(err)
})

//添加路由模块
app.use('/api', userRouter)
app.use('/my', userInfoRouter)
app.use('/my/article', artCateRouter)
app.use('/my/article', articleRouter)

// write your code here...


// 调用 app.listen 方法，指定端口号并启动web服务器
app.listen(80, function () {
    console.log('api server running at http://127.0.0.1:80')
})