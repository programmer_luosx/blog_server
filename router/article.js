const express = require('express')
// 验证数据的中间件
const expressJoi = require('@escook/express-joi')
const { add_article_schema, 
    get_articleList_schema, 
    del_article_schema,
    get_article_schema,
    update_article_schema,
} = require('../schema/article')
// 解析 formdata 格式表单数据的包
const multer = require('multer')
// 处理路径的核心模块
const path = require('path')

// 创建 multer 的实例对象，通过 dest 属性指定文件的存放路径
const upload = multer({ dest: path.join(__dirname, '../uploads') })

const router = express.Router()

const article_handler = require('../router_handler/article')

// 发布新文章的路由
// upload.single() 是一个局部生效的中间件，用来解析 FormData 格式的表单数据
// 将文件类型的数据，解析并挂载到 req.file 属性中
// 将文本类型的数据，解析并挂载到 req.body 属性中
//       先使用 multer 解析表单数据
//       再使用 expressJoi 对解析的表单数据进行验证
router.post('/add', upload.single('cover_img'), expressJoi(add_article_schema), article_handler.addArticle)

// 获取文章的列表
router.get('/list', expressJoi(get_articleList_schema), article_handler.getArticleList)

// 删除文章
router.post('/delete/:id', expressJoi(del_article_schema), article_handler.delArticle)

// 通过id获取文章
router.get('/:id', expressJoi(get_article_schema), article_handler.getArticle)

// 更新文章
router.post('/edit', expressJoi(update_article_schema), article_handler.updateArticle)

module.exports = router
