// 导入数据库操作模块
const db = require('../db/index')

// 根据分类的状态，获取所有未被删除的分类列表数据
// is_delete 为 0 表示没有被 标记为删除 的数据
const selectList = 'select * from ev_article_cate where is_delete=0 order by id asc'
// 查询 分类名称、分类别名 是否被占用
const selectByNameOrAlias = `select * from ev_article_cate where name=? or alias=?`
const insertOne = `insert into ev_article_cate set ?`
const deleteByid = `update ev_article_cate set is_delete=1 where id=?`
const selectByid = `select * from ev_article_cate where id=?`
// 查询 分类名称 与 分类别名 是否被占用的 SQL 语句
const selectByColumn = `select * from ev_article_cate where Id<>? and (name=? or alias=?)`
const updateByid = `update ev_article_cate set ? where Id=?`


// 获取文章分类列表数据的处理函数
exports.getArticleCates = (req, res) => {
    db.query(selectList, (err, results) => {
        if (err) return res.cc(err)
        res.send({
          status: 0,
          message: '获取文章分类列表成功！',
          data: results,
        })
    })
}

// 新增文章分类的处理函数
exports.addArticleCates = (req, res) => {
    db.query(selectByNameOrAlias, [req.body.name, req.body.alias], (err, results) => {
        if (err) return res.cc(err)

        // 分类名称 和 分类别名 都被占用
        if (results.length === 2) return res.cc('分类名称与别名被占用，请更换后重试！')
        if (results.length === 1 && results[0].name === req.body.name && results[0].alias === req.body.alias) return res.cc('分类名称与别名被占用，请更换后重试！')
        // 分类名称 或 分类别名 被占用
        if (results.length === 1 && results[0].name === req.body.name) return res.cc('分类名称被占用，请更换后重试！')
        if (results.length === 1 && results[0].alias === req.body.alias) return res.cc('分类别名被占用，请更换后重试！')
    
        // 新增文章分类
        db.query(insertOne, req.body, (err, results) => {
            if (err) return res.cc(err)
            if (results.affectedRows !== 1) return res.cc('新增文章分类失败！')
            // 新增文章分类成功
            res.cc('新增文章分类成功！', 0)
        })
    })
}

// 删除文章分类的处理函数
exports.deleteCateById = (req, res) => {
    db.query(deleteByid, req.params.id, (err, results) => {
        if (err) return res.cc(err)
        if (results.affectedRows !== 1) return res.cc('删除文章分类失败！')
        // 删除文章分类成功
        res.cc('删除文章分类成功！', 0)
    })
}

// 根据 Id 获取文章分类的处理函数
exports.getArtCateById = (req, res) => {
    db.query(selectByid, req.params.id, (err, results) => {
        if (err) return res.cc(err)
        if (results.length !== 1) return res.cc('获取文章分类数据失败！')
        res.send({
          status: 0,
          message: '获取文章分类数据成功！',
          data: results[0],
        })
    })
}

// 更新文章分类的处理函数
exports.updateCateById = (req, res) => {
    // 查重
    db.query(selectByColumn, [req.body.Id, req.body.name, req.body.alias], (err, results) => {
        if (err) return res.cc(err)
        // 分类名称 和 分类别名 都被占用
        if (results.length === 2) return res.cc('分类名称与别名被占用，请更换后重试！')
        if (results.length === 1 && results[0].name === req.body.name && results[0].alias === req.body.alias) return res.cc('分类名称与别名被占用，请更换后重试！')
        // 分类名称 或 分类别名 被占用
        if (results.length === 1 && results[0].name === req.body.name) return res.cc('分类名称被占用，请更换后重试！')
        if (results.length === 1 && results[0].alias === req.body.alias) return res.cc('分类别名被占用，请更换后重试！')
    
        // 更新文章分类
        db.query(updateByid, [req.body, req.body.Id], (err, results) => {
            if (err) return res.cc(err)
            if (results.affectedRows !== 1) return res.cc('更新文章分类失败！')
            // 更新文章分类
            res.cc('更新文章分类成功！', 0)
        })          
    })
}
