const db = require('../db/index')
// 导入处理路径的 path 核心模块
const path = require('path')

const insert = `insert into ev_articles set ?`
const selectListByCateOrState = `select * from ev_articles where cate_id=? and state=? limit ? offset ?`
const deleteByid = `update ev_articles set is_delete=1 where id=?`
const selectByid = `select * from ev_articles where id= ?`
const updateByid = `update ev_articles set ? where id=?`

// 发布新文章的处理函数
exports.addArticle = (req, res) => {
    // 手动判断是否上传了文章封面
  if (!req.file || req.file.fieldname !== 'cover_img')
    return res.cc('文章封面是必选参数！')
  
    const articleInfo = {
        // 标题、内容、状态、所属的分类Id
        ...req.body,
        // 文章封面在服务器端的存放路径
        cover_img: path.join('/uploads', req.file.filename),
        // 文章发布时间
        pub_date: new Date(),
        // 文章作者的Id
        author_id: req.user.id,
    }
    db.query(insert, articleInfo, (err, results) => {
        if (err) return res.cc(err)
        if (results.affectedRows !== 1) return res.cc('发布文章失败！')
        // 发布文章成功
        res.cc('发布文章成功', 0)
    })
}

// 获取文章的列表数据
// pagenum	页码值
// pagesize	每页显示多少条数据
exports.getArticleList = (req, res) => {
    const { cate_id, state, pagenum, pagesize } = req.body
    db.query(selectListByCateOrState, [cate_id, state, pagesize, +pagenum * +pagesize], (err, results)=>{
        if (err) return res.cc(err)
        if (results.affectedRows !== 1) return res.cc('查询失败！')
        res.cc('查询成功', results)
    })
}
  
// 删除文章
exports.delArticle = (req, res) => {
    const { id } = req.body
    db.query(deleteByid, id, (err, results)=>{
        if (err) return res.cc(err)
        if (results.affectedRows !== 1) return res.cc('删除失败！')
        res.cc('删除成功', 0)
    })
}

// 通过id获取文章详情
exports.getArticle = (req, res) => {
    const { id } = req.params
    db.query(selectByid, id, (err, results)=>{
        if (err) return res.cc(err)
        if (results.affectedRows !== 1) return res.cc('查询文章详情失败！')
        res.cc('查询文章详情成功', results[0])
    })
}

// 更新文章
exports.updateArticle = (req, res) => {
    db.query(updateByid, [req.body, req.body.id], (err, results)=>{
        if (err) return res.cc(err)
        if (results.affectedRows !== 1) return res.cc('更新文章失败！')
        res.cc('更新文章成功', 0)
    })
}
  
