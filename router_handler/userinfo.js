// 导入数据库操作模块
const db = require('../db/index')
const bcrypt = require('bcryptjs')

// 注意：为了防止用户的密码泄露，需要排除 password 字段
const selectByidNotpwd = `select id, username, nickname, email, user_pic from ev_users where id=?`
const updateByid = `update ev_users set ? where id=?`
const selectByid = `select * from ev_users where id=?`
const updatePwd = `update ev_users set password=? where id=?`
const updatePicByid = 'update ev_users set user_pic=? where id=?'


// 获取用户基本信息的处理函数
exports.getUserInfo = (req, res) => {
    db.query(selectByidNotpwd, req.user.id, (err,results)=>{
        if (err) return res.cc(err)
        if (results.length !== 1) return res.cc('获取用户信息失败！')
        res.send({
            status: 0,
            message: '获取用户基本信息成功！',
            data: results[0],
        })
    })
}

// 更新用户基本信息的处理函数
exports.updateUserInfo = (req, res) => {
    db.query(updateByid, [req.body, req.body.id], (err, results) => {
        if (err) return res.cc(err)
        if (results.affectedRows !== 1) return res.cc('修改用户基本信息失败！')
        // 修改用户信息成功
        return res.cc('修改用户基本信息成功！', 0)
    })      
}

// 重置密码的处理函数
exports.updatePassword = (req, res) => {
    // 查询用户是否存在
    db.query(selectByid, req.user.id, (err, results) => {
        if (err) return res.cc(err)
        if (results.length !== 1) return res.cc('用户不存在！')

        // 判断提交的旧密码是否正确
        const compareResult = bcrypt.compareSync(req.body.oldPwd, results[0].password)
        if (!compareResult) return res.cc('原密码错误！')
        // 对新密码进行 bcrypt 加密处理
        const newPwd = bcrypt.hashSync(req.body.newPwd, 10)

        // 更新用户的密码
        db.query(updatePwd, [newPwd, req.user.id], (err, results) => {
            if (err) return res.cc(err)
            if (results.affectedRows !== 1) return res.cc('更新密码失败！')
            // 更新密码成功
            res.cc('更新密码成功！', 0)
        })
    })
}

// 更新用户头像的处理函数
exports.updateAvatar = (req, res) => {
    db.query(updatePicByid, [req.body.avatar, req.user.id], (err, results) => {
        if (err) return res.cc(err)
        if (results.affectedRows !== 1) return res.cc('更新头像失败！')
        // 更新用户头像成功
        return res.cc('更新头像成功！', 0)
    })
}
