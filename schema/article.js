const joi = require('joi')

// 定义 标题、分类Id、内容、发布状态 的验证规则
const title = joi.string().required()
const cate_id = joi.number().integer().min(1).required()
const content = joi.string().required().allow('')
const state = joi.string().valid('已发布', '草稿').required()
// 分页参数
const pagenum = joi.number().required()
const pagesize = joi.number().required()

const id = joi.number().integer().min(1).required()
const cover_img = joi.string().dataUri().required()

// 验证规则对象 - 发布文章
exports.add_article_schema = {
    body: {
        title,
        cate_id,
        content,
        state,
    },
}

// 获取文章的列表数据
exports.get_articleList_schema = {
    body: {
        cate_id,
        state,
        pagenum,
        pagesize,
    },
}

// Id 删除文章
exports.del_article_schema = {
    body: {
        id,
    },
}

// Id 获取文章详情
exports.get_article_schema = {
    body: {
        id,
    },
}

// 更新文章
exports.update_article_schema = {
    body: {
        id,
        title,
        cate_id,
        content,
        state,
        cover_img,
    },
}